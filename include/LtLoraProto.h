#ifndef LORA_LT_PROTO_H
#define LORA_LT_PROTO_H

#include <stdint.h>

/*******************************************************************************
 * defines
 ******************************************************************************/

/* protocol defines */
#define LT_PROTO_VERSION    1

#define LT_MHDR_FRAME_TYPE_SHIFT                   5
#define LT_MHDR_FRAME_TYPE_MASK                    0xE0
#define LT_MHDR_FRAME_TYPE_UNCONFIRMED_UPLINK      0x2
#define LT_MHDR_FRAME_TYPE_UNCONFIRMED_DOWNLINK    0x3
#define LT_MHDR_FRAME_TYPE_CONFIRMED_UPLINK        0x4
#define LT_MHDR_FRAME_TYPE_CONFIRMED_DOWNLINK      0x5

#define LT_MHDR_FRAME_VERSION_SHIFT                0
#define LT_MHDR_FRAME_VERSION_MASK                 0x07

#define LT_FHDR_CTRL_FOPTS_LEN_SHIFT               0
#define LT_FHDR_CTRL_FOPTS_LEN_MASK                0x0F

/* MAC commands */
#define LT_MAC_CMD_LINK_CHECK_CID       0x02    /* node to getaway */
#define LT_MAC_CMD_DEV_STATUS_CID       0x06    /* gateway to node */
#define LT_MAC_CMD_DEV_SIGNAL_CID       0x80    /* gateway to node */

#define LT_MAC_CMD_LINK_CHECK_REQ_LEN   0
#define LT_MAC_CMD_LINK_CHECK_ANS_LEN   2       /* 1 octet RSSI, 1 octet SNR */
#define LT_MAC_CMD_DEV_STATUS_REQ_LEN   0
#define LT_MAC_CMD_DEV_STATUS_ANS_LEN   2       /* 1 octet battery, 1 octet SNR - DEPRECATED */
#define LT_MAC_CMD_DEV_SIGNAL_REQ_LEN   0
#define LT_MAC_CMD_DEV_SIGNAL_ANS_LEN   2       /* 1 octet RSSI, 1 octet SNR */

/* radio configuration */
#define LT_LORA_BAND               8681E5
#define LT_LORA_BANDWIDTH          125E3
#define LT_LORA_SPREADING_FACTOR   7
#define LT_LORA_CODING_RATE        5
#define LT_LORA_SYNC_WORD          0x56

/*******************************************************************************
 * macros
 ******************************************************************************/

#define LT_FRAME_PAYLOAD_HEADERS_LEN(a) (sizeof(a->mhdr) + \
                                         sizeof(a->mac_payload.fhdr) + \
                                         sizeof(a->mac_payload.frame_port))

#define LT_GET_MHDR_FRAME_TYPE(mhdr)        ((mhdr & LT_MHDR_FRAME_TYPE_MASK) >> LT_MHDR_FRAME_TYPE_SHIFT)
#define LT_SET_MHDR_FRAME_TYPE(mhdr, type)  (mhdr |= (type << LT_MHDR_FRAME_TYPE_SHIFT))
#define LT_SET_MHDR_FRAME_VERSION(mhdr)     (mhdr |= (LT_PROTO_VERSION << LT_MHDR_FRAME_VERSION_SHIFT))

#define LT_GET_FHDR_CTRL_FOPTS_LEN(fctrl)        ((fctrl & LT_FHDR_CTRL_FOPTS_LEN_MASK) >> LT_FHDR_CTRL_FOPTS_LEN_SHIFT)
#define LT_SET_FHDR_CTRL_FOPTS_LEN(fctrl, len)   (fctrl |= ((len & LT_FHDR_CTRL_FOPTS_LEN_MASK) << LT_FHDR_CTRL_FOPTS_LEN_SHIFT))

/*******************************************************************************
 * typedefs
 ******************************************************************************/

typedef struct s_lt_fhdr
{
    uint16_t    devaddr;
    uint8_t     fctrl;
    uint16_t    fcnt;
} __attribute__((packed)) t_lt_fhdr;

typedef struct s_lt_mac_payload
{
    t_lt_fhdr   fhdr;
    uint8_t     frame_port;
    uint8_t     frame_payload[1];
} __attribute__((packed)) t_lt_mac_payload;

typedef struct s_phy_payload
{
    uint8_t             mhdr;
    t_lt_mac_payload    mac_payload;
} __attribute__((packed)) t_lt_phy_payload;

#endif //LORA_LT_PROTO_H